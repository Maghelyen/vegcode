package com.maghelyen.vegcode.product.display

import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.Product
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * Created by Alena_Kabardinova on 6/12/2018.
 */
class ProductDisplayPresenterTest : Spek({
    val model by memoized { mock(ProductDisplayContract.Model::class.java) }
    val logger by memoized { mock(EventLogger::class.java) }

    val presenter by memoized { ProductDisplayPresenter(model, logger) }
    val view by memoized { mock(ProductDisplayContract.View::class.java) }

    describe("product display presenter") {

        context("when view attached") {

            context("when product is null") {

                beforeEachTest {
                    Mockito.`when`(model.getProductInfo())
                            .thenReturn( null )
                    presenter.attach(view)
                }

                it("should get product info from model") {
                    verify(model).getProductInfo()
                }

                it("should ask view to show not found theme") {
                    verify(view).setNotFoundProductTheme()
                }
            }

            context("when product exist") {

                context("when product is suitable") {

                    val product = Product(
                            "",
                            ArrayList(),
                            ArrayList(),
                            Product.Suitability.SUITABLE
                    )

                    beforeEachTest {
                        Mockito.`when`(model.getProductInfo())
                                .thenReturn(product)
                        presenter.attach(view)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should ask view to show suitable theme") {
                        verify(view).setSuitableProductTheme()
                    }
                }

                context("when product is unsuitable") {

                    val product = Product(
                            "",
                            ArrayList(),
                            ArrayList(),
                            Product.Suitability.UNSUITABLE
                    )

                    beforeEachTest {
                        Mockito.`when`(model.getProductInfo())
                                .thenReturn(product)
                        presenter.attach(view)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should ask view to show unsuitable theme") {
                        verify(view).setUnsuitableProductTheme()
                    }
                }

                context("when product is gray") {

                    val product = Product(
                            "",
                            ArrayList(),
                            ArrayList(),
                            Product.Suitability.GRAY
                    )

                    beforeEachTest {
                        Mockito.`when`(model.getProductInfo())
                                .thenReturn(product)
                        presenter.attach(view)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should ask view to show gray area theme") {
                        verify(view).setGrayAreaProductTheme()
                    }
                }

                context("when product has un unknown status") {

                    val product = Product(
                            "",
                            ArrayList(),
                            ArrayList(),
                            Product.Suitability.UNKNOWN
                    )

                    beforeEachTest {
                        Mockito.`when`(model.getProductInfo())
                                .thenReturn(product)
                        presenter.attach(view)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should log UnknownProductStatusException") {
                        verify(logger).logException(
                                any<AppErrors.UnknownProductStatusException>()
                        )
                    }
                }
            }
        }

        context("when initializes view") {

            beforeEachTest {
                presenter.attach(view)
            }

            context("when product is null") {

                beforeEachTest {
                    Mockito.`when`(model.getProductInfo())
                            .thenReturn( null )
                    presenter.initView()
                }

                it("should call for view initialisation") {
                    verify(view).init()
                }

                it("should get product info from model for a second time") {
                    verify(model, times(2)).getProductInfo()
                }

                it("should ask view to display product not found message") {
                    verify(view).displayProductNotFound()
                }
            }

            context("when product exist") {

                val name = "product name"
                val manufacturer = "product manufacturer"
                val country = "product country"
                val ingredients = arrayListOf("ing1.", "ing2 ,", "in g3", "ing4,", "ing5")
                val ingredientsString = "ing1., ing2 ,, in g3, ing4,, ing5"
                val issues = arrayListOf(
                        Product.Issue(0, "product issue 1", "", Product.Category.PRODUCT),
                        Product.Issue(3, "product issue 4", "", Product.Category.PRODUCT),
                        Product.Issue(4, "country issue 1", "", Product.Category.COUNTRY),
                        Product.Issue(5, "manufacturer issue 1", "", Product.Category.MANUFACTURER)
                )
                val product = Product(
                        name,
                        ingredients,
                        issues,
                        Product.Suitability.GRAY,
                        manufacturer,
                        country
                )

                beforeEachTest {
                    Mockito.`when`(model.getProductInfo())
                            .thenReturn(product)
                    presenter.initView()
                }

                it("should call for view initialisation") {
                    verify(view).init()
                }

                it("should get product info from model for a second time") {
                    verify(model, times(2)).getProductInfo()
                }

                it("should ask view to display product info") {
                    verify(view).displayProductInfo(
                            name,
                            manufacturer,
                            country,
                            ingredientsString,
                            issues
                    )
                }
            }
        }

        context("when edit button clicked") {

            beforeEachTest {
                presenter.attach(view)
                presenter.editButtonClicked()
            }

            it("should ask view to open edit screen") {
                verify(view).goToEditScreen()
            }
        }

        context("when new scan button clicked") {

            beforeEachTest {
                presenter.attach(view)
                presenter.newScanButtonClicked()
            }

            it("should ask view to open scan screen") {
                verify(view).goToScanScreen()
            }
        }
    }
})