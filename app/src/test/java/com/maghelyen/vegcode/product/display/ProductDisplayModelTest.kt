package com.maghelyen.vegcode.product.display

import com.maghelyen.vegcode.product.repo.ProductRepo
import com.nhaarman.mockito_kotlin.verify
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.Mockito.mock

/**
 * Created by Alena_Kabardinova on 6/12/2018.
 */
class ProductDisplayModelTest : Spek({
    val productData by memoized { mock(ProductRepo::class.java) }

    val model by memoized { ProductDisplayModel(productData) }

    describe("product display model") {

        context("when asked for product info") {

            beforeEachTest {
                model.getProductInfo()
            }

            it("should get product info from holder") {
                verify(productData).getProduct()
            }
        }
    }
})