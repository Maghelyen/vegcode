package com.maghelyen.vegcode.product.edit

import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.Product
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * Created by Alena_Kabardinova on 6/12/2018.
 */
class ProductEditPresenterTest : Spek({
    val model by memoized { mock(ProductEditContract.Model::class.java) }
    val logger by memoized { mock(EventLogger::class.java) }

    val presenter by memoized { ProductEditPresenter(model, logger) }
    val view by memoized { mock(ProductEditContract.View::class.java) }

    describe("product edit presenter") {

        beforeEachTest {
            presenter.attach(view)
        }

        context("when initializes view") {

            val allIssues = arrayListOf(
                    Product.Issue(0, "product issue 1", "", Product.Category.PRODUCT),
                    Product.Issue(1, "product issue 2", "", Product.Category.PRODUCT),
                    Product.Issue(2, "product issue 3", "", Product.Category.PRODUCT),
                    Product.Issue(3, "product issue 4", "", Product.Category.PRODUCT)
            )

            beforeEachTest {
                Mockito.`when`(model.getProductIssues())
                        .thenReturn( Single.just(allIssues) )
            }

            context("when product is null") {

                beforeEachTest {
                    Mockito.`when`(model.getProductInfo())
                            .thenReturn( null )
                    presenter.initView()
                }

                it("should call for view initialisation") {
                    verify(view).init()
                }

                it("should get product info from model") {
                    verify(model).getProductInfo()
                }

                it("shouldn't ask view to display product info") {
                    verify(view, never()).displayProductInfo(any(), any())
                }

                it("should get issues info from model") {
                    verify(model).getProductIssues()
                }

                it("should ask view to init issues") {
                    verify(view).initIssues(allIssues, ArrayList())
                }
            }

            context("when product exist") {

                val name = "product name"
                val ingredients = arrayListOf("ing1", "ing2", "ing3", "ing4", "ing5")
                val issues = arrayListOf(
                        Product.Issue(0, "product issue 1", "", Product.Category.PRODUCT),
                        Product.Issue(3, "product issue 4", "", Product.Category.PRODUCT),
                        Product.Issue(4, "country issue 1", "", Product.Category.COUNTRY),
                        Product.Issue(5, "manufacturer issue 1", "", Product.Category.MANUFACTURER)
                )
                val product = Product(
                        name,
                        ingredients,
                        issues,
                        Product.Suitability.UNKNOWN,
                        "",
                        ""
                )

                beforeEachTest {
                    Mockito.`when`(model.getProductInfo())
                            .thenReturn( product )
                    presenter.initView()
                }

                it("should call for view initialisation") {
                    verify(view).init()
                }

                it("should get product info from model") {
                    verify(model).getProductInfo()
                }

                it("should ask view to display product info") {
                    verify(view).displayProductInfo(name, ingredients)
                }

                it("should get issues info from model") {
                    verify(model).getProductIssues()
                }

                it("should ask view to init issues") {
                    verify(view).initIssues(allIssues, issues)
                }
            }
        }

        context("when product was edited several times") {

            beforeEachTest {
                presenter.productEdited()
                presenter.productEdited()
                presenter.productEdited()
                presenter.productEdited()
            }

            it("should ask view to enable save button once") {
                verify(view, times(1)).enableSaveButton()
            }
        }

        context("when leave dialog button was clicked") {

            beforeEachTest {
                presenter.leaveDialogBtnClicked()
            }

            it("should ask view to open display screen") {
                verify(view).goToDisplayProductScreen()
            }
        }

        context("when thank you dialog button was clicked") {

            beforeEachTest {
                presenter.thankYouDialogBtnClicked()
            }

            it("should ask view to open scan screen") {
                verify(view).goToScanScreen()
            }
        }

        context("when back button was clicked") {

            context("when product wasn't edited") {

                beforeEachTest {
                    presenter.onBackClicked()
                }

                it("should ask view to open display screen") {
                    verify(view).goToDisplayProductScreen()
                }
            }

            context("when product was edited") {

                beforeEachTest {
                    presenter.productEdited()
                    presenter.onBackClicked()
                }

                it("should ask view to show leave dialog") {
                    verify(view).showLeaveDialog()
                }
            }
        }

        context("when asked to search ingredients") {

            context("when server have found ingredients") {

                val ingredients = arrayListOf("ing1", "ing2", "ing3", "ing4", "ing5")

                beforeEachTest {
                    Mockito.`when`(model.searchIngredients(any()))
                            .thenReturn( Single.just(ingredients) )
                    presenter.onSearchIngredients("")
                }

                it("should ask view to display found ingredients") {
                    verify(view).displayFoundIngredients(ingredients)
                }
            }

            context("when server return any error") {

                beforeEachTest {
                    Mockito.`when`(model.searchIngredients(any()))
                            .thenReturn( Single.fromCallable { throw AppErrors.ServerException("code", "message") } )
                    presenter.onSearchIngredients("")
                }

                it("shouldn't ask view to display ingredients") {
                    verify(view, never()).displayFoundIngredients(any())
                }

                it("should log ServerException") {
                    verify(logger).logException(
                            any<AppErrors.ServerException>()
                    )
                }
            }

            context("when app version is deprecated") {

                beforeEachTest {
                    Mockito.`when`(model.searchIngredients(any()))
                            .thenReturn( Single.fromCallable { throw AppErrors.DeprecatedAppVersion() } )
                    presenter.onSearchIngredients("")
                }

                it("shouldn't ask view to display ingredients") {
                    verify(view, never()).displayFoundIngredients(any())
                }

                it("shouldn't log exceptions") {
                    verify(logger, never()).logException(any())
                }

                it("shouldn't ask view to handle exceptions") {
                    verify(view, never()).handleError(any())
                }

                it("shouldn't ask view to show update app message") {
                    verify(view, never()).showUpdateAppMessage()
                }
            }

            context("when the internet connection is turned off") {

                beforeEachTest {
                    Mockito.`when`(model.searchIngredients(any()))
                            .thenReturn( Single.fromCallable { throw AppErrors.NetworkConnectionException() } )
                    presenter.onSearchIngredients("")
                }

                it("shouldn't ask view to display ingredients") {
                    verify(view, never()).displayFoundIngredients(any())
                }

                it("shouldn't log exceptions") {
                    verify(logger, never()).logException(any())
                }

                it("shouldn't ask view to handle exceptions") {
                    verify(view, never()).handleError(any())
                }
            }
        }

        context("when save product button was clicked") {

            val name = "product name"
            val ingredients = arrayListOf("ing1", "ing2", "ing3", "ing4", "ing5")
            val issues = arrayListOf(
                    Product.Issue(0, "product issue 1", "", Product.Category.PRODUCT),
                    Product.Issue(3, "product issue 4", "", Product.Category.PRODUCT),
                    Product.Issue(4, "country issue 1", "", Product.Category.COUNTRY),
                    Product.Issue(5, "manufacturer issue 1", "", Product.Category.MANUFACTURER)
            )
            val product = Product(
                    name,
                    ingredients,
                    issues
            )
            val comment = "some comment"

            context("when current product is null") {

                beforeEachTest {
                    Mockito.`when`(model.getProductInfo())
                            .thenReturn( null )
                }

                context("when product is saved successfully") {

                    beforeEachTest {
                        Mockito.`when`(model.saveProductInfo(product, comment))
                                .thenReturn( Single.just(true) )
                        presenter.onSaveProductClicked(name, ingredients, issues, comment)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should ask model to save product info") {
                        verify(model).saveProductInfo(product, comment)
                    }

                    it("should ask view to show thank you dialog") {
                        verify(view).showThankYouDialog()
                    }
                }
            }

            context("when current product exist") {

                val currentName = "current name"
                val currentIngredients = arrayListOf("c ing1", "c ing2", "c ing3", "c ing4", "c ing5")
                val currentManufacturer = "current manufacturer"
                val currentCountry = "current country"
                val currentIssues = arrayListOf(
                        Product.Issue(0, "c product issue 1", "", Product.Category.PRODUCT),
                        Product.Issue(3, "c product issue 4", "", Product.Category.PRODUCT),
                        Product.Issue(4, "c country issue 1", "", Product.Category.COUNTRY),
                        Product.Issue(5, "c manufacturer issue 1", "", Product.Category.MANUFACTURER)
                )
                val currentProduct = Product(
                        currentName,
                        currentIngredients,
                        currentIssues,
                        Product.Suitability.GRAY,
                        currentManufacturer,
                        currentCountry
                )

                val newProduct = Product(
                        name,
                        ingredients,
                        issues,
                        Product.Suitability.GRAY,
                        currentManufacturer,
                        currentCountry
                )

                beforeEachTest {
                    Mockito.`when`(model.getProductInfo())
                            .thenReturn( currentProduct )
                }

                context("when product is saved successfully") {

                    beforeEachTest {
                        Mockito.`when`(model.saveProductInfo(newProduct, comment))
                                .thenReturn( Single.just(true) )
                        presenter.onSaveProductClicked(name, ingredients, issues, comment)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should ask model to save product info") {
                        verify(model).saveProductInfo(newProduct, comment)
                    }

                    it("should ask view to show thank you dialog") {
                        verify(view).showThankYouDialog()
                    }
                }

                context("when server returns any error while saving product") {

                    beforeEachTest {
                        Mockito.`when`(model.saveProductInfo(newProduct, comment))
                                .thenReturn( Single.fromCallable { throw AppErrors.ServerException("code", "message") } )
                        presenter.onSaveProductClicked(name, ingredients, issues, comment)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should ask model to save product info") {
                        verify(model).saveProductInfo(newProduct, comment)
                    }

                    it("should log ServerException") {
                        verify(logger).logException(
                                any<AppErrors.ServerException>()
                        )
                    }

                    it("should ask view to handle error") {
                        verify(view).handleError(any<AppErrors.ServerException>())
                    }
                }

                context("when app version is deprecated") {

                    beforeEachTest {
                        Mockito.`when`(model.saveProductInfo(newProduct, comment))
                                .thenReturn( Single.fromCallable { throw AppErrors.DeprecatedAppVersion() } )
                        presenter.onSaveProductClicked(name, ingredients, issues, comment)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should ask model to save product info") {
                        verify(model).saveProductInfo(newProduct, comment)
                    }

                    it("shouldn't log exceptions") {
                        verify(logger, never()).logException(any())
                    }

                    it("should ask view to show update app message") {
                        verify(view).showUpdateAppMessage()
                    }
                }

                context("when the internet connection is turned off") {

                    beforeEachTest {
                        Mockito.`when`(model.saveProductInfo(newProduct, comment))
                                .thenReturn( Single.fromCallable { throw AppErrors.NetworkConnectionException() } )
                        presenter.onSaveProductClicked(name, ingredients, issues, comment)
                    }

                    it("should get product info from model") {
                        verify(model).getProductInfo()
                    }

                    it("should ask model to save product info") {
                        verify(model).saveProductInfo(newProduct, comment)
                    }

                    it("shouldn't log exceptions") {
                        verify(logger, never()).logException(any())
                    }

                    it("should ask view to handle NetworkConnectionException") {
                        verify(view).handleError(any<AppErrors.NetworkConnectionException>())
                    }
                }
            }
        }
    }
})