package com.maghelyen.vegcode.product.edit

import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.interactors.Interactor
import com.maghelyen.vegcode.interactors.Provider
import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.product.repo.IssuesRepo
import com.maghelyen.vegcode.product.repo.ProductRepo
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * Created by Alena_Kabardinova on 6/12/2018.
 */
class ProductEditModelTest : Spek({
    val issuesData by memoized { mock(IssuesRepo::class.java) }
    val productData by memoized { mock(ProductRepo::class.java) }
    val interactorProvider by memoized { mock(Provider::class.java) }

    val model by memoized { ProductEditModel(issuesData, productData, interactorProvider) }

    describe("product edit model") {

        context("when asked for product info") {

            beforeEachTest {
                model.getProductInfo()
            }

            it("should get product info from holder") {
                verify(productData).getProduct()
            }
        }

        context("when asked for product issues") {

            val interactor = mock(Interactor::class.java)

            context("when the internet connection is turned off") {

                beforeEachTest {
                    Mockito.`when`(interactorProvider.getInteractor())
                            .thenAnswer { throw AppErrors.NetworkConnectionException() }
                    model.getProductIssues().subscribe( {}, {} )
                }

                it("should ask product issues from issues repo") {
                    verify(issuesData).getProductIssues()
                }
            }

            context("when server returns product issues") {

                val issues = arrayListOf(
                        Product.Issue(0, "product issue 1", "", Product.Category.PRODUCT),
                        Product.Issue(3, "product issue 4", "", Product.Category.PRODUCT),
                        Product.Issue(4, "country issue 1", "", Product.Category.COUNTRY),
                        Product.Issue(5, "manufacturer issue 1", "", Product.Category.MANUFACTURER)
                )

                beforeEachTest {
                    Mockito.`when`(interactorProvider.getInteractor())
                            .thenReturn( interactor )
                    Mockito.`when`(interactor.getProductIssues())
                            .thenReturn( Single.just(issues) )
                    model.getProductIssues().subscribe( {}, {} )
                }

                it("should save product issues in issues repo") {
                    verify(issuesData).saveProductIssues(issues)
                }
            }

            context("when server returns any error") {

                beforeEachTest {
                    Mockito.`when`(interactorProvider.getInteractor())
                            .thenReturn( interactor )
                    Mockito.`when`(interactor.getProductIssues())
                            .thenAnswer { throw AppErrors.ServerException("code", "message") }
                    model.getProductIssues().subscribe( {}, {} )
                }

                it("should ask product issues from issues repo") {
                    verify(issuesData).getProductIssues()
                }
            }
        }
    }
})