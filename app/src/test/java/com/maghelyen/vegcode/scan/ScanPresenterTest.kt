package com.maghelyen.vegcode.scan

import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.google.zxing.ResultPoint
import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.Product
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.Mockito
import org.mockito.Mockito.mock
import kotlin.test.assertEquals

/**
 * Created by Alena_Kabardinova on 5/12/2018.
 */
class ScanPresenterTest : Spek({
    val model by memoized { mock(ScanContract.Model::class.java) }
    val logger by memoized { mock(EventLogger::class.java) }

    val presenter by memoized { ScanPresenter(model, logger) }
    val view by memoized { mock(ScanContract.View::class.java) }

    describe("scan presenter") {

        beforeEachTest {
            presenter.attach(view)
        }

        context("when initializes view") {

            beforeEachTest {
                presenter.initView()
            }

            it("should call for view initialisation") {
                verify(view).init()
            }
        }

        context("when asked for scan listener") {

            var listener : ZXingScannerView.ResultHandler? = null

            beforeEachTest {
                listener = presenter.getScanListener()
            }

            it("should return itself") {
                assertEquals(listener, presenter)
            }
        }

        context("when auto focus set to true") {

            beforeEachTest {
                presenter.onAutoFocusChecked(true)
            }

            it("should save true value in model") {
                verify(model).saveAutoFocus(true)
            }
        }

        context("when auto focus set to false") {

            beforeEachTest {
                presenter.onAutoFocusChecked(false)
            }

            it("should save false value in model") {
                verify(model).saveAutoFocus(false)
            }
        }

        context("when asked for auto focus value") {

            beforeEachTest {
                presenter.getAutoFocusValue()
            }

            it("should ask for auto focus value from model") {
                verify(model).getAutoFocus()
            }
        }

        context("when barcode scanned") {

            val barcode = "1234567890123"
            val barcodeScanResult = Result(
                    barcode,
                    byteArrayOf(0),
                    arrayOf(ResultPoint(0f, 0f)),
                    BarcodeFormat.EAN_13
            )

            context("when barcode is null") {

                beforeEachTest {
                    presenter.handleResult(null)
                }

                it("should log BarcodeScanException") {
                    verify(logger).logException(
                            any<AppErrors.BarcodeScanException>()
                    )
                }

                it("should stop progress in view") {
                    verify(view).stopProgress()
                }

                it("should tell view to handle BarcodeScanException") {
                    verify(view).handleError(
                            any<AppErrors.BarcodeScanException>()
                    )
                }
            }

            context("when barcode belong to a suitable product") {

                val product = Product(
                        "",
                        ArrayList(),
                        ArrayList(),
                        Product.Suitability.SUITABLE
                )

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.just(product) )

                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should open suitable screen") {
                    verify(view).goToSuitableScreen()
                }
            }

            context("when barcode belong to an unsuitable product") {

                val product = Product(
                        "",
                        ArrayList(),
                        ArrayList(),
                        Product.Suitability.UNSUITABLE
                )

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.just(product) )
                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should open suitable screen") {
                    verify(view).goToUnsuitableScreen()
                }
            }

            context("when barcode belong to a gray product") {

                val product = Product(
                        "",
                        ArrayList(),
                        ArrayList(),
                        Product.Suitability.GRAY
                )

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.just(product) )
                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should open suitable screen") {
                    verify(view).goToGrayAreaScreen()
                }
            }

            context("when barcode belong to a product with unknown status") {

                val product = Product(
                        "",
                        ArrayList(),
                        ArrayList()
                )

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.just(product) )
                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should log UnknownProductStatusException") {
                    verify(logger).logException(
                            any<AppErrors.UnknownProductStatusException>()
                    )
                }

                it("should stop progress in view") {
                    verify(view).stopProgress()
                }

                it("should tell view to handle UnknownProductStatusException") {
                    verify(view).handleError(
                            any<AppErrors.UnknownProductStatusException>()
                    )
                }
            }

            context("when barcode belong to a product that isn't in database") {

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.fromCallable { throw AppErrors.ProductNotFoundException() } )
                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should open product not found screen") {
                    verify(view).goToNotFoundScreen()
                }
            }

            context("when barcode has a wrong format") {

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.fromCallable { throw AppErrors.WrongBarcodeFormatException() } )
                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should stop progress in view") {
                    verify(view).stopProgress()
                }

                it("should tell view to handle WrongBarcodeFormatException") {
                    verify(view).handleError(
                            any<AppErrors.WrongBarcodeFormatException>()
                    )
                }
            }

            context("when the internet connection is turned off") {

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.fromCallable { throw AppErrors.NetworkConnectionException() } )
                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should stop progress in view") {
                    verify(view).stopProgress()
                }

                it("should tell view to handle NetworkConnectionException") {
                    verify(view).handleError(
                            any<AppErrors.NetworkConnectionException>()
                    )
                }
            }

            context("when app version is deprecated") {

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.fromCallable { throw AppErrors.DeprecatedAppVersion() } )
                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should stop progress in view") {
                    verify(view).stopProgress()
                }

                it("should tell view to show update app message") {
                    verify(view).showUpdateAppMessage()
                }
            }

            context("when server returns any error") {

                beforeEachTest {
                    Mockito.`when`(model.getProductFromBarcode(barcode))
                            .thenReturn( Single.fromCallable { throw AppErrors.ServerException("code", "message") } )
                    presenter.handleResult(barcodeScanResult)
                }

                it("should start progress in view") {
                    verify(view).startProgress()
                }

                it("should log barcode scanned event") {
                    verify(logger).logBarcodeScannedEvent(barcode)
                }

                it("should ask for product from model") {
                    verify(model).getProductFromBarcode(barcode)
                }

                it("should log ServerException") {
                    verify(logger).logException(
                            any<AppErrors.ServerException>()
                    )
                }

                it("should stop progress in view") {
                    verify(view).stopProgress()
                }

                it("should tell view to handle ServerException") {
                    verify(view).handleError(
                            any<AppErrors.ServerException>()
                    )
                }
            }
        }
    }
})