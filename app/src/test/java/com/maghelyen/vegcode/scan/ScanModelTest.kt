package com.maghelyen.vegcode.scan

import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.interactors.Interactor
import com.maghelyen.vegcode.interactors.Provider
import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.product.repo.ProductRepo
import com.maghelyen.vegcode.settings.SettingsRepo
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * Created by Alena_Kabardinova on 5/12/2018.
 */
class ScanModelTest : Spek({
    val productData by memoized { mock(ProductRepo::class.java) }
    val interactorProvider by memoized { mock(Provider::class.java) }

    val model by memoized { ScanModel(productData, interactorProvider) }

    describe("scan model") {

        context("when asked for product by barcode") {

            val interactor = mock(Interactor::class.java)
            val barcode = "1234567890123"

            context("when the internet connection is turned off") {

                beforeEachTest {
                    Mockito.`when`(interactorProvider.getInteractor())
                            .thenAnswer { throw AppErrors.NetworkConnectionException() }
                    model.getProductFromBarcode(barcode)
                }

                it("should save barcode in holder") {
                    verify(productData).setBarcode(barcode)
                }

                it("shouldn't save new product value") {
                    verify(productData, never()).setProduct(ArgumentMatchers.any())
                }
            }

            context("when server returns a product") {

                val product = Product(
                        "",
                        ArrayList(),
                        ArrayList(),
                        Product.Suitability.SUITABLE
                )

                beforeEachTest {
                    Mockito.`when`(interactorProvider.getInteractor())
                            .thenReturn( interactor )
                    Mockito.`when`(interactor.getProductByBarcode(barcode))
                            .thenReturn( Single.just(product) )
                    model.getProductFromBarcode(barcode).subscribe()
                }

                it("should save barcode in holder") {
                    verify(productData).setBarcode(barcode)
                }

                it("should save product in holder") {
                    verify(productData).setProduct(product)
                }
            }

            context("when server returns any error") {

                beforeEachTest {
                    Mockito.`when`(interactorProvider.getInteractor())
                            .thenReturn( interactor )
                    Mockito.`when`(interactor.getProductByBarcode(barcode))
                            .thenReturn( Single.fromCallable { throw AppErrors.ProductNotFoundException() } )
                    model.getProductFromBarcode(barcode).subscribe({ }, { })
                }

                it("should save barcode in holder") {
                    verify(productData).setBarcode(barcode)
                }

                it("should save null product in holder") {
                    verify(productData).setProduct(null)
                }
            }
        }
    }
})