package com.maghelyen.vegcode.scan.di

import javax.inject.Scope

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ScanScope