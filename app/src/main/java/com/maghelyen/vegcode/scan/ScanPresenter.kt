package com.maghelyen.vegcode.scan

import android.annotation.SuppressLint
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.advertising.Advert
import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.Product
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/19/2018.
 */
class ScanPresenter @Inject constructor(
        private val model : ScanContract.Model,
        private val eventLogger : EventLogger
) : ScanContract.Presenter, BarcodeCallback {

    private var view : ScanContract.View? = null

    override fun attach(view : ScanContract.View) {
        this.view = view
    }

    override fun detach() {
        view = null
    }

    override fun initView() {
        view?.init()
    }

    override fun getScanListener(): BarcodeCallback {
        return this
    }

    @SuppressLint("CheckResult")
    override fun prepareAdvertForScreenSize(size: String) {
        model.getAdvert(size)
                .subscribe(
                        {
                            view?.showBanner(it)
                        },
                        {
                            handleError(it, false)
                        }
                )
    }

    @SuppressLint("CheckResult")
    override fun onAdvertClicked(ad: Advert) {
        view?.openLink(ad.link)
        model.advertClicked(ad)
                .doOnError {
                    handleError(it, false)
                }
    }

    @SuppressLint("CheckResult")
    override fun barcodeResult(barcode: BarcodeResult?) {
        if (barcode == null) {
            handleError(AppErrors.BarcodeScanException(), true)
            return
        }

        view?.startProgress()

        eventLogger.logBarcodeScannedEvent(barcode.text)

        model.getProductFromBarcode(barcode.text)
                .subscribe(
                        {
                            updateView(it.suitable)
                        },
                        {
                            handleError(it, true)
                        }
                )
    }

    override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
    }

    private fun handleError(t : Throwable, updateView : Boolean) {
        when (t::class) {
            AppErrors.ProductNotFoundException::class -> {
                if (updateView) view?.goToNotFoundScreen()
            }
            AppErrors.DeprecatedAppVersion::class -> {
                if (updateView) {
                    view?.stopProgress()
                    view?.showUpdateAppMessage()
                }
            }
            AppErrors.WrongBarcodeFormatException::class,
            AppErrors.NetworkConnectionException::class -> {
                if (updateView) {
                    view?.stopProgress()
                    view?.handleError(t)
                }
            }
            else -> {
                eventLogger.logException(t)
                if (updateView) {
                    view?.stopProgress()
                    view?.handleError(t)
                }
            }
        }
    }

    private fun updateView(suitable : Product.Suitability) {
        when (suitable) {
            Product.Suitability.SUITABLE -> view?.goToSuitableScreen()
            Product.Suitability.UNSUITABLE -> view?.goToUnsuitableScreen()
            Product.Suitability.GRAY -> view?.goToGrayAreaScreen()
            else -> handleError(AppErrors.UnknownProductStatusException(), true)
        }
    }
}