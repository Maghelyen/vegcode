package com.maghelyen.vegcode.scan

import com.journeyapps.barcodescanner.BarcodeCallback
import com.maghelyen.vegcode.advertising.Advert
import com.maghelyen.vegcode.contracts.IPresenter
import com.maghelyen.vegcode.contracts.IView
import com.maghelyen.vegcode.product.Product
import io.reactivex.Single

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
interface ScanContract {
    interface View : IView {
        fun startProgress()
        fun stopProgress()
        fun goToSuitableScreen()
        fun goToUnsuitableScreen()
        fun goToGrayAreaScreen()
        fun goToNotFoundScreen()
        fun showUpdateAppMessage()
        fun handleError(throwable : Throwable)
        fun showBanner(ad : Advert)
        fun openLink(link : String)
    }

    interface Presenter : IPresenter<View> {
        fun getScanListener() : BarcodeCallback
        fun prepareAdvertForScreenSize(size: String)
        fun onAdvertClicked(ad : Advert)
    }

    interface Model {
        fun getProductFromBarcode(barcode : String) : Single<Product>
        fun getAdvert(size: String) : Single<Advert>
        fun advertClicked(ad : Advert) : Single<Boolean>
    }
}