package com.maghelyen.vegcode.scan.di

import com.maghelyen.vegcode.ui.scan.ScanActivity
import dagger.Subcomponent

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Subcomponent(modules = [(ScanModule::class)])
@ScanScope
interface ScanComponent {
    fun inject(scanActivity : ScanActivity)
}