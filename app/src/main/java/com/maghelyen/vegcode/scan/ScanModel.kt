package com.maghelyen.vegcode.scan

import com.maghelyen.vegcode.advertising.Advert
import com.maghelyen.vegcode.interactors.Provider
import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.product.repo.ProductRepo
import com.maghelyen.vegcode.settings.SettingsRepo
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
class ScanModel @Inject constructor(
        private val productData : ProductRepo,
        private val interactorProvider : Provider
) : ScanContract.Model {

    private val spaceName = "scan"

    override fun getProductFromBarcode(barcode : String) : Single<Product> {
//        val code = "4606979004313" // грибная колбаса с кармином
//        val code = "4810397002862" // сыр не по вегану
//        val code = "4607036203328" // кабачковая икра по вегану
//        val code = "4607024085677" // картофельный крахмал по вегану
//        val code = "123456" // ошибка

        productData.setBarcode(barcode)
        return try {
            interactorProvider.getInteractor().getProductByBarcode(barcode)
                    .doOnSuccess {
                        productData.setProduct(it)
                    }
                    .doOnError {
                        productData.setProduct(null)
                    }
        } catch (e : Exception) {
            Single.fromCallable { throw e }
        }
    }

    override fun getAdvert(size: String) : Single<Advert> {
        return try {
            interactorProvider.getInteractor().getAdvert(spaceName, size)
        } catch (e: Exception) {
            Single.fromCallable { throw e }
        }
    }

    override fun advertClicked(ad : Advert) : Single<Boolean> {
        return try {
            interactorProvider.getInteractor().advertClicked(spaceName, ad.id)
        } catch (e: Exception) {
            Single.fromCallable { throw e }
        }
    }
}