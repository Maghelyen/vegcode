package com.maghelyen.vegcode.scan.di

import com.maghelyen.vegcode.interactors.Provider
import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.repo.ProductRepo
import com.maghelyen.vegcode.scan.ScanContract
import com.maghelyen.vegcode.scan.ScanModel
import com.maghelyen.vegcode.scan.ScanPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Module
class ScanModule {

    @Provides
    @ScanScope
    internal fun provideScanPresenter(
            model : ScanContract.Model,
            eventLogger : EventLogger
    ) : ScanContract.Presenter =
        ScanPresenter(model, eventLogger)

    @Provides
    @ScanScope
    internal fun provideScanModel(
            productData : ProductRepo,
            provider : Provider) : ScanContract.Model =
        ScanModel(productData, provider)
}