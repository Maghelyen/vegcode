package com.maghelyen.vegcode.settings.di

import android.content.Context
import com.maghelyen.vegcode.di.AppScope
import com.maghelyen.vegcode.settings.AppPrefSettings
import com.maghelyen.vegcode.settings.SettingsRepo
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 5/30/2018.
 */
@Module
class SettingsModule {

    @Provides
    @AppScope
    fun provideSettings(context : Context) : SettingsRepo = AppPrefSettings(context)
}