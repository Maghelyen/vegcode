package com.maghelyen.vegcode.settings

import com.maghelyen.vegcode.product.Product

/**
 * Created by Alena_Kabardinova on 6/12/2018.
 */
interface SettingsRepo {
    fun getProductIssues() : ArrayList<Product.Issue>
    fun saveProductIssues(issues : ArrayList<Product.Issue>)
    fun getUserId() : String
    fun saveUserId(userId : String)
}