package com.maghelyen.vegcode.settings

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.maghelyen.vegcode.product.Product

/**
 * Created by Alena_Kabardinova on 5/30/2018.
 */
class AppPrefSettings(private val context : Context) : SettingsRepo {
    companion object {
        const val BASE_APP_SETTINGS = "BaseAppSettings"
        const val ISSUES_SETTINGS = "Issues"
        const val USER_SETTINGS = "User"

        const val PRODUCT_ISSUES_KEY = "ProductIssues"
        const val USER_ID_KEY = "UserId"
    }

    override fun getProductIssues() : ArrayList<Product.Issue> {
        val json = getIssuesSettings().getString(PRODUCT_ISSUES_KEY, "")
        val listType = object : TypeToken<ArrayList<Product.Issue>>() { }.type
        var issues : ArrayList<Product.Issue>? = Gson().fromJson(json, listType)

        if (issues == null) {
            issues = ArrayList()
        }

        return issues
    }

    override fun saveProductIssues(issues : ArrayList<Product.Issue>) {
        getIssuesSettings()
                .edit()
                .putString(PRODUCT_ISSUES_KEY, Gson().toJson(issues))
                .apply()
    }

    override fun getUserId() : String {
        return getUserSettings().getString(USER_ID_KEY, "")
    }

    override fun saveUserId(userId : String) {
        getUserSettings()
                .edit()
                .putString(USER_ID_KEY, userId)
                .apply()
    }

    private fun getBaseAppSettings() = context.getSharedPreferences(BASE_APP_SETTINGS, Context.MODE_PRIVATE)
    private fun getIssuesSettings() = context.getSharedPreferences(ISSUES_SETTINGS, Context.MODE_PRIVATE)
    private fun getUserSettings() = context.getSharedPreferences(USER_SETTINGS, Context.MODE_PRIVATE)
}