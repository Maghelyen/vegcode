package com.maghelyen.vegcode.user.di

import com.maghelyen.vegcode.di.AppScope
import com.maghelyen.vegcode.settings.SettingsRepo
import com.maghelyen.vegcode.user.UserData
import com.maghelyen.vegcode.user.UserRepo
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 2/09/2019.
 */
@Module
class UserModule {

    @Provides
    @AppScope
    fun provideUser(settings : SettingsRepo) : UserRepo = UserData(settings)
}