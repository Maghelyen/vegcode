package com.maghelyen.vegcode.user

import com.maghelyen.vegcode.settings.SettingsRepo
import java.util.*

/**
 * Created by Alena_Kabardinova on 2/09/2019.
 */
class UserData(appSettings : SettingsRepo) : UserRepo {

    private var userId : String

    init {
        userId = appSettings.getUserId()
        if (userId.isEmpty()) {
            userId = UUID.randomUUID().toString()
            appSettings.saveUserId(userId)
        }
    }

    override fun getUserId() : String {
        return userId
    }
}