package com.maghelyen.vegcode.user

/**
 * Created by Alena_Kabardinova on 2/09/2019.
 */
interface UserRepo {
    fun getUserId() : String
}