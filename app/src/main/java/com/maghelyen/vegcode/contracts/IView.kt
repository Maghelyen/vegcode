package com.maghelyen.vegcode.contracts

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
interface IView {
    fun init()
}