package com.maghelyen.vegcode.contracts

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
interface IPresenter<in T : IView> {
    fun attach(view : T)
    fun initView()
    fun detach()
}