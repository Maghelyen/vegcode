package com.maghelyen.vegcode.product.display.di

import com.maghelyen.vegcode.ui.product.ProductDisplayActivity
import dagger.Subcomponent

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Subcomponent(modules = [(ProductDisplayModule::class)])
@ProductDisplayScope
interface ProductDisplayComponent {
    fun inject(productDisplayActivity : ProductDisplayActivity)
}