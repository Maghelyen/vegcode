package com.maghelyen.vegcode.product.edit

import android.annotation.SuppressLint
import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.Product
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/19/2018.
 */
class ProductEditPresenter @Inject constructor(
        private val model : ProductEditContract.Model,
        private val eventLogger : EventLogger
) : ProductEditContract.Presenter {
    private var view : ProductEditContract.View? = null
    private var productEdited = false

    override fun attach(view : ProductEditContract.View) {
        this.view = view
    }

    override fun detach() {
        view = null
    }

    override fun initView() {
        view?.init()

        val product = model.getProductInfo()

        val productIssues = ArrayList<Product.Issue>()

        if (product != null) {

            view?.displayProductInfo(
                    product.name,
                    product.ingredients
            )

            productIssues.addAll(product.issues)
        }

        model.getProductIssues()
                .subscribe(Consumer { view?.initIssues(it, productIssues) })
    }

    override fun productEdited() {
        if (!productEdited) {
            productEdited = true
            view?.enableSaveButton()
        }
    }

    override fun leaveDialogBtnClicked() {
        view?.goToDisplayProductScreen()
    }

    override fun thankYouDialogBtnClicked() {
        view?.goToScanScreen()
    }

    override fun onBackClicked() {
        if (productEdited) {
            view?.showLeaveDialog()
        } else {
            view?.goToDisplayProductScreen()
        }
    }

    @SuppressLint("CheckResult")
    override fun onSearchIngredients(searchQuery: String) {
        model.searchIngredients(searchQuery)
                .subscribe(
                        {
                            view?.displayFoundIngredients(it)
                        },
                        {
                            handleError(it, false)
                        }
                )
    }

    @SuppressLint("CheckResult")
    override fun onSaveProductClicked(
            name : String,
            ingredients : ArrayList<String>,
            issues : ArrayList<Product.Issue>,
            comment : String
    ) {
        val currentProduct = model.getProductInfo()
        val product = when (currentProduct) {
            null -> Product(
                    name = name,
                    ingredients = ingredients,
                    issues = issues
            )
            else -> {
                currentProduct.copy(
                        name = name,
                        ingredients = ingredients,
                        issues = issues
                )
            }
        }

        model.saveProductInfo(product, comment)
                .subscribe(
                        {
                            view?.showThankYouDialog()
                        },
                        {
                            handleError(it, true)
                        }
                )
    }

    private fun handleError(t : Throwable, updateView : Boolean) {
        when (t::class) {
            AppErrors.DeprecatedAppVersion::class -> {
                if (updateView) view?.showUpdateAppMessage()
            }
            AppErrors.NetworkConnectionException::class -> {
                if (updateView) view?.handleError(t)
            }
            else -> {
                eventLogger.logException(t)
                if (updateView) view?.handleError(t)
            }
        }
    }
}