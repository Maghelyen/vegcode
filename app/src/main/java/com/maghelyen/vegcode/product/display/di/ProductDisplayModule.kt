package com.maghelyen.vegcode.product.display.di

import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.display.ProductDisplayContract
import com.maghelyen.vegcode.product.display.ProductDisplayModel
import com.maghelyen.vegcode.product.display.ProductDisplayPresenter
import com.maghelyen.vegcode.product.repo.ProductRepo
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Module
class ProductDisplayModule {

    @Provides
    @ProductDisplayScope
    internal fun provideProductEditPresenter(
            model : ProductDisplayContract.Model,
            eventLogger : EventLogger
    ) : ProductDisplayContract.Presenter =
            ProductDisplayPresenter(model, eventLogger)

    @Provides
    @ProductDisplayScope
    internal fun provideProductDisplayModel(productData : ProductRepo) : ProductDisplayContract.Model =
            ProductDisplayModel(productData)
}