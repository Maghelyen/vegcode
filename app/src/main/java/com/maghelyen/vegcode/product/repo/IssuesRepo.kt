package com.maghelyen.vegcode.product.repo

import com.maghelyen.vegcode.product.Product

interface IssuesRepo {
    fun getProductIssues() : ArrayList<Product.Issue>?
    fun saveProductIssues(issues : ArrayList<Product.Issue>)
}