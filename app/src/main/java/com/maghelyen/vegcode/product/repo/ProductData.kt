package com.maghelyen.vegcode.product.repo

import com.maghelyen.vegcode.product.Product

/**
 * Created by Alena_Kabardinova on 4/01/2018.
 */
data class ProductData(var b : String = "", var p: Product? = null) : ProductRepo {
    override fun getBarcode(): String {
        return b
    }

    override fun setBarcode(barcode: String) {
        b = barcode
    }

    override fun getProduct(): Product? {
        return p
    }

    override fun setProduct(product: Product?) {
        p = product
    }
}