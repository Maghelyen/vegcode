package com.maghelyen.vegcode.product.edit.di

import javax.inject.Scope

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ProductEditScope