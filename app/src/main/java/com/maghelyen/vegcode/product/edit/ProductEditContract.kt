package com.maghelyen.vegcode.product.edit

import com.maghelyen.vegcode.contracts.IPresenter
import com.maghelyen.vegcode.contracts.IView
import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.ui.inputs.EditableChipInput
import io.reactivex.Single

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
interface ProductEditContract {
    interface View : IView {
        fun displayProductInfo(
                name : String,
                ingredients : ArrayList<String>
        )
        fun displayFoundIngredients(ingredients : ArrayList<String>)
        fun initIssues(allIssues: ArrayList<Product.Issue>, productIssues: ArrayList<Product.Issue>)
        fun showLeaveDialog()
        fun showThankYouDialog()
        fun goToDisplayProductScreen()
        fun goToScanScreen()
        fun enableSaveButton()
        fun showUpdateAppMessage()
        fun handleError(throwable : Throwable)
    }

    interface Presenter : IPresenter<View> {
        fun productEdited()
        fun onBackClicked()
        fun leaveDialogBtnClicked()
        fun thankYouDialogBtnClicked()
        fun onSearchIngredients(searchQuery : String)
        fun onSaveProductClicked(
                name : String,
                ingredients : ArrayList<String>,
                issues : ArrayList<Product.Issue>,
                comment : String
        )
    }

    interface Model {
        fun getProductInfo() : Product?
        fun saveProductInfo(product : Product, comment : String) : Single<Boolean>
        fun searchIngredients(searchQuery : String) : Single<ArrayList<String>>
        fun getProductIssues() : Single<ArrayList<Product.Issue>>
    }
}