package com.maghelyen.vegcode.product.display

import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.Product
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/19/2018.
 */
class ProductDisplayPresenter @Inject constructor(
        private var model : ProductDisplayContract.Model,
        private val eventLogger : EventLogger
) : ProductDisplayContract.Presenter {

    private var view : ProductDisplayContract.View? = null

    override fun attach(view : ProductDisplayContract.View) {
        this.view = view
        setViewTheme()
    }

    override fun detach() {
        view = null
    }

    override fun initView() {
        view?.init()

        val product : Product? = model.getProductInfo()

        if (product == null) {
            view?.displayProductNotFound()
            return
        }

        val stringSeparator = ", "
        val issues = ArrayList<Product.Issue>()
        issues.addAll(product.issues)
        view?.displayProductInfo(
                product.name,
                product.manufacturer,
                product.country,
                product.ingredients.joinToString(stringSeparator),
                issues
        )
    }

    private fun setViewTheme() {
        val product : Product? = model.getProductInfo()

        if (product == null) {
            view?.setNotFoundProductTheme()
            return
        }

        when (product.suitable) {
            Product.Suitability.SUITABLE -> view?.setSuitableProductTheme()
            Product.Suitability.UNSUITABLE -> view?.setUnsuitableProductTheme()
            Product.Suitability.GRAY -> view?.setGrayAreaProductTheme()
            else -> handleError(AppErrors.UnknownProductStatusException())
        }
    }

    private fun handleError(t : Throwable) {
        eventLogger.logException(t)
    }

    override fun editButtonClicked() {
        view?.goToEditScreen()
    }

    override fun newScanButtonClicked() {
        view?.goToScanScreen()
    }
}