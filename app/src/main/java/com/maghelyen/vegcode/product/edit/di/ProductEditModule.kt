package com.maghelyen.vegcode.product.edit.di

import com.maghelyen.vegcode.interactors.Provider
import com.maghelyen.vegcode.logging.EventLogger
import com.maghelyen.vegcode.product.edit.ProductEditContract
import com.maghelyen.vegcode.product.edit.ProductEditModel
import com.maghelyen.vegcode.product.edit.ProductEditPresenter
import com.maghelyen.vegcode.product.repo.IssuesData
import com.maghelyen.vegcode.product.repo.IssuesRepo
import com.maghelyen.vegcode.product.repo.ProductRepo
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Module
class ProductEditModule {

    @Provides
    @ProductEditScope
    internal fun provideProductEditPresenter(
            model : ProductEditContract.Model,
            eventLogger : EventLogger
    ) : ProductEditContract.Presenter =
            ProductEditPresenter(model, eventLogger)

    @Provides
    @ProductEditScope
    internal fun provideProductEditModel(
            issuesData: IssuesRepo,
            productData : ProductRepo,
            provider: Provider
    ) : ProductEditContract.Model =
            ProductEditModel(issuesData, productData, provider)
}