package com.maghelyen.vegcode.product

import com.google.gson.annotations.SerializedName

/**
 * Created by Alena_Kabardinova on 4/01/2018.
 */
data class Product(
        @SerializedName("name") val name : String,
        @SerializedName("ingredients") val ingredients: ArrayList<String>,
        @SerializedName("issues") val issues : ArrayList<Issue>,
        @SerializedName("suitable") val suitable : Suitability = Suitability.UNKNOWN,
        @SerializedName("manufacturer") val manufacturer : String = "",
        @SerializedName("country") val country: String = ""
) {
    data class Issue(
            @SerializedName("id") val id : Int,
            @SerializedName("issue") val name : String,
            @SerializedName("comment") val comment : String = "",
            @SerializedName("category") val category : Category
    ) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other?.javaClass != javaClass) return false

            other as Issue

            return other.id == this.id && other.category == this.category
        }

        override fun hashCode(): Int {
            var result = 17
            result = 31 * result + id.hashCode()
            result = 31 * result + category.hashCode()
            return result
        }
    }

    enum class Category {
        @SerializedName("ingredients") INGREDIENTS,
        @SerializedName("product") PRODUCT,
        @SerializedName("country") COUNTRY,
        @SerializedName("manufacturer") MANUFACTURER,
    }

    enum class Suitability {
        @SerializedName("green") SUITABLE,
        @SerializedName("red") UNSUITABLE,
        @SerializedName("gray") GRAY,
        @SerializedName("unknown") UNKNOWN,
    }
}