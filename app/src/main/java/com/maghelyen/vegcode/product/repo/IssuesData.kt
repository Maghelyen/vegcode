package com.maghelyen.vegcode.product.repo

import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.settings.SettingsRepo

/**
 * Created by Alena_Kabardinova on 5/30/2018.
 */
class IssuesData(private val appSettings: SettingsRepo) : IssuesRepo {

    val pIssues : ArrayList<Product.Issue> = ArrayList()

    override fun getProductIssues() : ArrayList<Product.Issue>? {
        if (pIssues.isEmpty()) {
            pIssues.addAll(appSettings.getProductIssues())
        }

        return pIssues
    }

    override fun saveProductIssues(issues : ArrayList<Product.Issue>) {
        pIssues.clear()
        pIssues.addAll(issues)
        appSettings.saveProductIssues(issues)
    }
}