package com.maghelyen.vegcode.product.display

import com.maghelyen.vegcode.contracts.IPresenter
import com.maghelyen.vegcode.contracts.IView
import com.maghelyen.vegcode.product.Product

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
interface ProductDisplayContract {
    interface View : IView {
        fun setSuitableProductTheme()
        fun setUnsuitableProductTheme()
        fun setGrayAreaProductTheme()
        fun setNotFoundProductTheme()
        fun displayProductInfo(
                name : String,
                manufacturer : String,
                country: String,
                ingredients : String,
                issues : ArrayList<Product.Issue>
        )
        fun displayProductNotFound()
        fun goToEditScreen()
        fun goToScanScreen()
    }

    interface Presenter : IPresenter<View> {
        fun editButtonClicked()
        fun newScanButtonClicked()
    }

    interface Model {
        fun getProductInfo() : Product?
    }
}