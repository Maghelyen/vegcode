package com.maghelyen.vegcode.product.edit

import com.maghelyen.vegcode.interactors.Provider
import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.product.repo.IssuesRepo
import com.maghelyen.vegcode.product.repo.ProductRepo
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
class ProductEditModel @Inject constructor(
        private val issuesData: IssuesRepo,
        private val productData : ProductRepo,
        private val interactorProvider : Provider
) : ProductEditContract.Model {

    override fun getProductInfo() : Product? = productData.getProduct()

    override fun saveProductInfo(product : Product, comment : String) : Single<Boolean> {
        return try {
            interactorProvider.getInteractor().saveProduct(productData.getBarcode(), product, comment)
        } catch (e: Exception) {
            Single.fromCallable { throw e }
        }
    }

    override fun searchIngredients(searchQuery: String): Single<ArrayList<String>> {
        return try {
            interactorProvider.getInteractor().searchIngredients(searchQuery)
        } catch (e: Exception) {
            Single.fromCallable { throw e }
        }
    }

    override fun getProductIssues(): Single<ArrayList<Product.Issue>> {
        return try {
            interactorProvider.getInteractor().getProductIssues()
                    .onErrorReturn({
                        return@onErrorReturn issuesData.getProductIssues()
                    })
                    .doOnSuccess {
                        issuesData.saveProductIssues(it)
                    }
        } catch (e: Exception) {
            Single.fromCallable { issuesData.getProductIssues() }
        }
    }
}