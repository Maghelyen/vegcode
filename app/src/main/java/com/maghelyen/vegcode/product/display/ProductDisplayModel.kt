package com.maghelyen.vegcode.product.display

import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.product.repo.ProductRepo
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
class ProductDisplayModel @Inject constructor(
        private val productData : ProductRepo
) : ProductDisplayContract.Model {
    override fun getProductInfo(): Product? = productData.getProduct()
}