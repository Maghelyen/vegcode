package com.maghelyen.vegcode.product.repo.di

import com.maghelyen.vegcode.di.AppScope
import com.maghelyen.vegcode.product.repo.IssuesData
import com.maghelyen.vegcode.product.repo.IssuesRepo
import com.maghelyen.vegcode.product.repo.ProductData
import com.maghelyen.vegcode.product.repo.ProductRepo
import com.maghelyen.vegcode.settings.SettingsRepo
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 4/2/2018.
 */
@Module
class RepoModule {
    @Provides
    @AppScope
    fun provideProductHolder() : ProductRepo = ProductData()

    @Provides
    @AppScope
    fun provideIssuesRepo(settings: SettingsRepo) : IssuesRepo = IssuesData(settings)
}