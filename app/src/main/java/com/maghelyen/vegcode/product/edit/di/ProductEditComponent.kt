package com.maghelyen.vegcode.product.edit.di

import com.maghelyen.vegcode.ui.product.ProductEditActivity
import dagger.Subcomponent

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Subcomponent(modules = [(ProductEditModule::class)])
@ProductEditScope
interface ProductEditComponent {
    fun inject(productEditActivity : ProductEditActivity)
}