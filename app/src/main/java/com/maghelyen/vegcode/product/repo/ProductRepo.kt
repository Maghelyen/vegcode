package com.maghelyen.vegcode.product.repo

import com.maghelyen.vegcode.product.Product

interface ProductRepo {
    fun getBarcode() : String
    fun setBarcode(barcode : String)
    fun getProduct() : Product?
    fun setProduct(product : Product?)
}