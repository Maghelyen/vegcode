package com.maghelyen.vegcode.advertising

import com.google.gson.annotations.SerializedName

/**
 * Created by Alena_Kabardinova on 11/01/2018.
 */
data class Advert(
        @SerializedName("adId") val id : Int,
        @SerializedName("banner") val banner : String,
        @SerializedName("link") val link: String
        )