package com.maghelyen.vegcode

import android.support.multidex.MultiDexApplication
import com.maghelyen.vegcode.di.AppComponent
import com.maghelyen.vegcode.di.AppModule
import com.maghelyen.vegcode.di.DaggerAppComponent

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
class VegCodeApplication : MultiDexApplication() {

    val appComponent : AppComponent by lazy {
        DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }
}