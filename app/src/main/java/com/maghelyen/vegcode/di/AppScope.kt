package com.maghelyen.vegcode.di

import javax.inject.Scope

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope