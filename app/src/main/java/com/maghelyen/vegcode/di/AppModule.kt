package com.maghelyen.vegcode.di

import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 5/27/2018.
 */
@Module
class AppModule(val context: Context) {

    @Provides
    @AppScope
    fun provideContext() : Context = context
}