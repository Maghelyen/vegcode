package com.maghelyen.vegcode.di

import com.maghelyen.vegcode.interactors.di.DataModule
import com.maghelyen.vegcode.logging.LoggingModule
import com.maghelyen.vegcode.network.ApiModule
import com.maghelyen.vegcode.product.repo.di.RepoModule
import com.maghelyen.vegcode.product.display.di.ProductDisplayComponent
import com.maghelyen.vegcode.product.edit.di.ProductEditComponent
import com.maghelyen.vegcode.scan.di.ScanComponent
import com.maghelyen.vegcode.settings.di.SettingsModule
import com.maghelyen.vegcode.user.di.UserModule
import dagger.Component

/**
 * Created by Alena_Kabardinova on 3/27/2018.
 */
@Component(modules = [
    RepoModule::class,
    SettingsModule::class,
    ApiModule::class,
    DataModule::class,
    AppModule::class,
    LoggingModule::class,
    UserModule::class])
@AppScope
interface AppComponent {
    fun newProductDisplayComponent() : ProductDisplayComponent
    fun newProductEditComponent() : ProductEditComponent
    fun newScanComponent() : ScanComponent
}