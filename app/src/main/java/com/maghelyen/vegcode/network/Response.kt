package com.maghelyen.vegcode.network

import com.google.gson.annotations.SerializedName
import com.maghelyen.vegcode.advertising.Advert
import com.maghelyen.vegcode.product.Product

/**
 * Created by Alena_Kabardinova on 5/23/2018.
 */
data class Response(
        @SerializedName("success") val success : Boolean,
        @SerializedName("product") val product : Product? = null,
        @SerializedName("issues") val issues : ArrayList<Product.Issue>? = null,
        @SerializedName("ingredients") val ingredients : ArrayList<String>? = null,
        @SerializedName("ad") val advert : Advert? = null,
        @SerializedName("error") val error : Error? = null

) {
    data class Error(
            @SerializedName("code") val code : String,
            @SerializedName("message") val message : String
    )
}