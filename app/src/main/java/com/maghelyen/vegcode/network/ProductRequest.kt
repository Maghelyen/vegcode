package com.maghelyen.vegcode.network

import com.google.gson.annotations.SerializedName
import com.maghelyen.vegcode.product.Product

data class ProductRequest(
        @SerializedName("barcode") val barcode : String,
        @SerializedName("comment") val comment : String,
        @SerializedName("product") val product : Product,
        @SerializedName("token") val token : String,
        @SerializedName("versionNumber") val versionNumber : Int,
        @SerializedName("source") val source : String
)