package com.maghelyen.vegcode.network

import com.google.gson.GsonBuilder
import com.maghelyen.vegcode.BuildConfig
import com.maghelyen.vegcode.di.AppScope
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Alena_Kabardinova on 5/09/2018.
 */
@Module
class ApiModule {

    @Provides
    @AppScope
    fun provideBaseUrl() : String = "https://vegcode.ru"

    @Provides
    @AppScope
    fun provideRetrofit(baseUrl : String): Retrofit {

        val okHttpBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpBuilder.addInterceptor(httpLoggingInterceptor)
        }

//        okHttpBuilder.addInterceptor({
//            val request = it.request()
//            val url = request.url().newBuilder()
//                    .addQueryParameter(API_KEY, BuildConfig.API_KEY)
//                    .build()
//            it.proceed(request.newBuilder().url(url).build())
//        })

        val gsonBuilder = GsonBuilder()
                .setLenient()
                .create()

        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpBuilder.build())
                .build()
    }

    @Provides
    @AppScope
    fun provideProductService(retrofit: Retrofit): VegCodeAPIService =
            retrofit.create(VegCodeAPIService::class.java)
}