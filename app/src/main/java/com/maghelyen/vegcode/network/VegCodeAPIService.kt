package com.maghelyen.vegcode.network

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Alena_Kabardinova on 5/09/2018.
 */
interface VegCodeAPIService {

    @GET("api/getproduct")
    fun getProduct(
            @Query("token") token : String,
            @Query("category") category : String,
            @Query("barcode") barcode : String,
            @Query("versionNumber") versionNumber : Int,
            @Query("source") source : String
    ) : Single<Response>

    @GET("api/getissues")
    fun getIssues(
            @Query("token") token : String,
            @Query("category") category : String,
            @Query("issuecategory") issueCategory : String,
            @Query("versionNumber") versionNumber : Int,
            @Query("source") source : String
    ) : Single<Response>

    @GET("api/searchingredients")
    fun getIngredients(
            @Query("token") token : String,
            @Query("search") category : String,
            @Query("versionNumber") versionNumber : Int,
            @Query("source") source : String
    ) : Single<Response>

    @POST("api/addproduct")
    fun addProduct(
        @Body product: ProductRequest
    ) : Single<Response>

    @GET("api/getad")
    fun getAd(
            @Query("token") token : String,
            @Query("userid") userId : String,
            @Query("space") space : String,
            @Query("size") size : String,
            @Query("versionNumber") versionNumber : Int,
            @Query("source") source : String
    ) : Single<Response>

    @GET("api/adlinkopened")
    fun adLinkOpened(
            @Query("token") token : String,
            @Query("userid") userId : String,
            @Query("adId") adId : Int,
            @Query("space") space : String,
            @Query("versionNumber") versionNumber : Int,
            @Query("source") source : String
    ) : Single<Response>
}