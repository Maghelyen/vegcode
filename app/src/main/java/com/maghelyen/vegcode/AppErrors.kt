package com.maghelyen.vegcode

/**
 * Created by Alena_Kabardinova on 3/30/2018.
 */
interface AppErrors {

    class BarcodeScanException : Exception()

    class ProductNotFoundException : Exception()

    class ProductInModerationException : Exception()

    class WrongBarcodeFormatException : Exception()

    class UnknownProductStatusException : Exception()

    class ServerException(val code : String, message : String) : Exception(message)

    class NetworkConnectionException : Exception()

    class DeprecatedAppVersion : Exception()
}