package com.maghelyen.vegcode.logging

import android.util.Log

/**
 * Created by Alena_Kabardinova on 6/10/2018.
 */
class LogcatLogger : EventLogger {

    override fun logException(throwable : Throwable) {
        Log.e("MyLogs", "Non critical error: ", throwable)
    }

    override fun logBarcodeScannedEvent(barcode : String) {
        Log.d("MyLogs", "Barcode scanned: $barcode")
    }
}