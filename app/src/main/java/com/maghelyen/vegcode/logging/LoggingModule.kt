package com.maghelyen.vegcode.logging

import android.content.Context
import com.maghelyen.vegcode.BuildConfig
import com.maghelyen.vegcode.di.AppScope
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 6/10/2018.
 */
@Module
class LoggingModule {

    @Provides
    @AppScope
    fun provideCrashLogger(context: Context) : EventLogger =
            if (BuildConfig.DEBUG) LogcatLogger() else FirebaseLogger(context)
}