package com.maghelyen.vegcode.logging

/**
 * Created by Alena_Kabardinova on 6/10/2018.
 */
interface EventLogger {
    fun logException(throwable : Throwable)
    fun logBarcodeScannedEvent(barcode : String)
}