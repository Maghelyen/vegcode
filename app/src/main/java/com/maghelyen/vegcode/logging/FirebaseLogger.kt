package com.maghelyen.vegcode.logging

import android.content.Context
import android.os.Bundle
import com.crashlytics.android.Crashlytics
import com.google.firebase.analytics.FirebaseAnalytics

/**
 * Created by Alena_Kabardinova on 6/10/2018.
 */
class FirebaseLogger(val context : Context) : EventLogger {

    override fun logException(throwable : Throwable) {
        Crashlytics.logException(throwable)
    }

    override fun logBarcodeScannedEvent(barcode : String) {
        val eventName = "barcode_scanned"
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, barcode)
        FirebaseAnalytics.getInstance(context).logEvent(eventName, bundle)
    }
}