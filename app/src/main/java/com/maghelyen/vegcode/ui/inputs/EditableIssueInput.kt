package com.maghelyen.vegcode.ui.inputs

import android.content.Context
import android.util.AttributeSet
import android.widget.CheckBox
import com.maghelyen.vegcode.product.Product

/**
 * Created by Alena_Kabardinova on 4/08/2018.
 */
class EditableIssueInput : CheckBox, EditableInput<EditableIssueInput.CheckedIssue> {
    private var listener : EditableInput.OnEditListener? = null
    private var listen = true
    private lateinit var value : CheckedIssue

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setValue(value: CheckedIssue) {
        this.value = value

        listen = false
        isChecked = value.checked
        text = value.issue.name
        listen = true
    }

    override fun getValue(): CheckedIssue = value

    override fun setOnEditListener(listener: EditableInput.OnEditListener) {
        this.listener = listener
    }

    override fun setChecked(checked: Boolean) {
        if (isChecked != checked && listen) {
            value.checked = checked
            listener?.onEdit()
        }

        super.setChecked(checked)
    }

    data class CheckedIssue(
            var checked : Boolean,
            var issue : Product.Issue
    )
}