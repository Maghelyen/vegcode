package com.maghelyen.vegcode.ui.inputs

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.util.AttributeSet

/**
 * Created by Alena_Kabardinova on 4/08/2018.
 */
class EditableTextInput : TextInputEditText, EditableInput<String> {
    private var listener : EditableInput.OnEditListener? = null
    private var listen = true

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setValue(value: String) {
        listen = false
        setText(value)
        listen = true
    }

    override fun getValue(): String = text.toString()

    override fun setOnEditListener(listener: EditableInput.OnEditListener) {
        this.listener = listener
    }

    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        if (listen) {
            listener?.onEdit()
        }
    }
}