package com.maghelyen.vegcode.ui.product

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.R
import com.maghelyen.vegcode.VegCodeApplication
import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.product.edit.ProductEditContract
import com.maghelyen.vegcode.ui.ActivityNavigator
import com.maghelyen.vegcode.ui.inputs.EditableChipInput
import com.maghelyen.vegcode.ui.inputs.EditableInput
import com.maghelyen.vegcode.ui.inputs.EditableIssueInput
import kotlinx.android.synthetic.main.activity_product_edit.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/19/2018.
 */
class ProductEditActivity : AppCompatActivity(),
        ProductEditContract.View,
        View.OnClickListener,
        EditableInput.OnEditListener,
        EditableChipInput.OnQuerySuggestionsListener{
    companion object {
        const val TAG = "ProductEditActivity"
        fun getIntent(context: Context): Intent = Intent(context, ProductEditActivity::class.java)
    }

    @Inject
    lateinit var presenter: ProductEditContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as VegCodeApplication).appComponent.newProductEditComponent().inject(this)
        presenter.attach(this)

        setContentView(R.layout.activity_product_edit)
        presenter.initView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun onBackPressed() {
        presenter.onBackClicked()
    }

    override fun init() {
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar_title.setText(R.string.app_name)

        ingredients_ci.setOnQueryListener(this)
        ingredients_ci.setOnEditListener(this)
        name_et.setOnEditListener(this)
        comment_et.setOnEditListener(this)
        save_product_btn.setOnClickListener(this)
    }

    override fun displayProductInfo(
            name : String,
            ingredients : ArrayList<String>) {
        name_et.setValue(name)
        ingredients_ci.setValue(ingredients)
    }

    override fun initIssues(allIssues: ArrayList<Product.Issue>, productIssues: ArrayList<Product.Issue>) {
        allIssues.forEach {
            val issueView : EditableIssueInput =
                    LayoutInflater.from(this)
                            .inflate(R.layout.issue_edit_item_view, issues_container, false) as EditableIssueInput
            val lp = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            lp.topMargin = resources.getDimension(R.dimen.big_margin).toInt()
            issueView.layoutParams = lp
            issueView.setOnEditListener(this)

            val checkedIssue = EditableIssueInput.CheckedIssue(productIssues.contains(it), it)
            issueView.setValue(checkedIssue)

            issues_container.addView(issueView)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                presenter.onBackClicked()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.save_product_btn -> {

                val issues : ArrayList<Product.Issue> = ArrayList()
                for (index in 0 until (issues_container as ViewGroup).childCount) {
                    val editableIssueInput = (issues_container as ViewGroup).getChildAt(index) as EditableIssueInput
                    val checkedIssue = editableIssueInput.getValue()
                    if (checkedIssue.checked) {
                        issues.add(checkedIssue.issue)
                    }
                }

                presenter.onSaveProductClicked(
                        name_et.getValue(),
                        ingredients_ci.getValue(),
                        issues,
                        comment_et.getValue()
                )
            }
        }
    }

    override fun onEdit() {
        presenter.productEdited()
    }

    override fun onQuerySuggestions(query: String, input: EditableChipInput) {
        when (input.id) {
            R.id.ingredients_ci -> {
                presenter.onSearchIngredients(query)
            }
        }
    }

    override fun displayFoundIngredients(ingredients: ArrayList<String>) {
        ingredients_ci.setSuggestions(ingredients)
    }

    override fun enableSaveButton() {
        save_product_btn.isEnabled = true
    }

    override fun showLeaveDialog() {
        AlertDialog.Builder(this)
                .setTitle(R.string.changes_not_saved_dialog_title)
                .setMessage(R.string.changes_not_saved_dialog_text)
                .setPositiveButton(R.string.changes_not_saved_dialog_button_ok, { _, _ ->
                    presenter.leaveDialogBtnClicked()
                })
                .setNegativeButton(R.string.changes_not_saved_dialog_button_cancel, { _, _ ->
                })
                .setCancelable(false)
                .create()
                .show()
    }

    override fun showThankYouDialog() {
        AlertDialog.Builder(this)
                .setTitle(R.string.changes_saved_dialog_title)
                .setMessage(R.string.changes_saved_dialog_text)
                .setPositiveButton(R.string.changes_saved_dialog_button_ok, { _, _ ->
                    presenter.thankYouDialogBtnClicked()
                })
                .setCancelable(false)
                .create()
                .show()
    }

    override fun goToDisplayProductScreen() {
        ActivityNavigator.goToParentActivity(this)
    }

    override fun goToScanScreen() {
        ActivityNavigator.goToScanActivity(this, true)
    }

    override fun showUpdateAppMessage() {
        AlertDialog.Builder(this)
                .setTitle(R.string.error_deprecated_version_title)
                .setMessage(R.string.error_deprecated_version)
                .setPositiveButton(R.string.error_dialog_button_download_update) { dialog, _ ->
                    ActivityNavigator.goToPlayStore(this)
                    dialog.cancel()
                }
                .setNegativeButton(R.string.error_dialog_button_cancel) { dialog, _ ->
                    dialog.cancel()
                }
                .setCancelable(false)
                .show()
    }

    override fun handleError(throwable: Throwable) {
        val message = when (throwable::class) {
            AppErrors.NetworkConnectionException::class -> {
                getString(R.string.error_network_connection)
            }
            AppErrors.ServerException::class -> {
                when ((throwable as AppErrors.ServerException).code) {
                    "403" -> getString(R.string.error_token)
                    else -> getString(R.string.error_server_edit)
                }
            }
            else -> {
                getString(R.string.error_server_edit)
            }
        }

        AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.error_dialog_button_ok) { dialog, _ ->
                    dialog.cancel()
                }
                .setCancelable(false)
                .show()
    }
}