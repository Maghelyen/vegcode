package com.maghelyen.vegcode.ui.inputs

/**
 * Created by Alena_Kabardinova on 4/08/2018.
 */
interface EditableInput<T : Any> {
    interface OnEditListener {
        fun onEdit()
    }
    fun setValue(value : T)
    fun getValue() : T
    fun setOnEditListener(listener: OnEditListener)
}