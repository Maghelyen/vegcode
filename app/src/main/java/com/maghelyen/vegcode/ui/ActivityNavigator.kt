package com.maghelyen.vegcode.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.app.NavUtils
import com.maghelyen.vegcode.ui.product.ProductDisplayActivity
import com.maghelyen.vegcode.ui.product.ProductEditActivity
import com.maghelyen.vegcode.ui.scan.ScanActivity

/**
 * Created by Alena_Kabardinova on 4/11/2018.
 */
class ActivityNavigator {
    companion object {
        private const val clearTopFlags : Int = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        fun goToScanActivity(context : Context, clearTop : Boolean = false) {
            val intent = ScanActivity.getIntent(context)
            if (clearTop) intent.flags = clearTopFlags
            context.startActivity(intent)
        }

        fun goToProductDisplayActivity(context : Context, clearTop : Boolean = false) {
            val intent = ProductDisplayActivity.getIntent(context)
            if (clearTop) intent.flags = clearTopFlags
            context.startActivity(intent)
        }

        fun goToProductEditActivity(context : Context, clearTop : Boolean = false) {
            val intent = ProductEditActivity.getIntent(context)
            if (clearTop) intent.flags = clearTopFlags
            context.startActivity(intent)
        }

        fun goToParentActivity(activity : Activity) {
            NavUtils.navigateUpFromSameTask(activity)
        }

        fun goToPlayStore(context : Context) {
            val appPackageName = context.packageName
            try {
                context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
            } catch (e: android.content.ActivityNotFoundException) {
                context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
            }

        }

        fun goToLink(context : Context, link : String) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
            context.startActivity(browserIntent)
        }
    }
}