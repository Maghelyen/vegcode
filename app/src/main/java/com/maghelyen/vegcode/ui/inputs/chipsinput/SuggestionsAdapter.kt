package com.maghelyen.vegcode.ui.inputs.chipsinput

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * Created by Alena_Kabardinova on 5/17/2018.
 */
class SuggestionsAdapter(
        private val suggestions : List<String>,
        private val listener : SuggestionsListener
) : RecyclerView.Adapter<SuggestionsAdapter.SuggestionItemViewHolder>() {
    interface SuggestionsListener {
        fun onSuggestionClick(suggestion: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestionItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(android.R.layout.simple_dropdown_item_1line, parent, false)
        return SuggestionItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return suggestions.size
    }

    override fun onBindViewHolder(holder: SuggestionItemViewHolder, position: Int) {
        holder.bindItems(suggestions[position])
    }

    inner class SuggestionItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(suggestion: String) {
            val suggestionText = itemView.findViewById(android.R.id.text1) as TextView
            suggestionText.text = suggestion

            suggestionText.setOnClickListener({
                listener.onSuggestionClick(suggestion)
            })
        }
    }
}