package com.maghelyen.vegcode.ui.product

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.maghelyen.vegcode.R
import com.maghelyen.vegcode.product.Product

/**
 * Created by Alena_Kabardinova on 5/13/2018.
 */
class IssuesAdapter(private val issues : List<Product.Issue>) : RecyclerView.Adapter<IssuesAdapter.IssueItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssueItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.issue_item_view, parent, false)
        return IssueItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return issues.size
    }

    override fun onBindViewHolder(holder: IssueItemViewHolder, position: Int) {
        holder.bindItems(issues[position])
    }

    class IssueItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var infoIsShown = false

        fun bindItems(issue: Product.Issue) {
            val issueName = itemView.findViewById(R.id.issue_name_tv) as TextView
            val issueDesc = itemView.findViewById(R.id.issue_desc_tv) as TextView
            val infoIcon = itemView.findViewById(R.id.info_iv) as ImageView

            issueName.text = issue.name

            if (issue.comment.isNotEmpty()) {
                issueDesc.text = issue.comment
                infoIcon.visibility = VISIBLE

                infoIcon.setOnClickListener({
                    if (infoIsShown) {
                        issueDesc.visibility = GONE
                    } else {
                        issueDesc.visibility = VISIBLE
                    }
                    infoIsShown = !infoIsShown
                })
            } else {
                infoIcon.visibility = GONE
            }
        }
    }
}