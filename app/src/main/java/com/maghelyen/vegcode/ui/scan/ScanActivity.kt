package com.maghelyen.vegcode.ui.scan

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.CompoundButton
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.R
import com.maghelyen.vegcode.VegCodeApplication
import com.maghelyen.vegcode.advertising.Advert
import com.maghelyen.vegcode.scan.ScanContract
import com.maghelyen.vegcode.ui.ActivityNavigator
import com.transitionseverywhere.*
import kotlinx.android.synthetic.main.activity_scan.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/19/2018.
 */
class ScanActivity : AppCompatActivity(), ScanContract.View, CompoundButton.OnCheckedChangeListener {
    companion object {
        const val TAG = "ScanActivity"
        fun getIntent(context : Context) : Intent = Intent(context, ScanActivity::class.java)
    }

    @Inject lateinit var presenter : ScanContract.Presenter

    private val CAMERA_PERMISSIONS_REQUEST = 0

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)

        (application as VegCodeApplication).appComponent.newScanComponent().inject(this)
        presenter.attach(this)

        setContentView(R.layout.activity_scan)
        presenter.initView()
    }

    override fun onStart() {
        super.onStart()
        requestCameraPermission()
    }

    override fun onResume() {
        super.onResume()
        scan_view.resume()
    }
    
    override fun onPause() {
        super.onPause()
        scan_view.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun init() {
        setSupportActionBar(toolbar)

        toolbar_title.setText(R.string.app_name)

        presenter.prepareAdvertForScreenSize(getString(R.string.size_name))
    }

    private fun requestCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSIONS_REQUEST)
        } else {
            initScanView()
        }
    }

    private fun initScanView() {
        scan_frame.visibility = VISIBLE

        scan_view.decodeContinuous(presenter.getScanListener())
        scan_view.setStatusText(getString(R.string.empty))

        flash_tb.setOnCheckedChangeListener(this)

        scan_view.resume()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == CAMERA_PERMISSIONS_REQUEST) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                initScanView()
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                Snackbar.make(transitions_container, R.string.reason_for_camera_permission, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.error_dialog_button_ok) {
                            requestCameraPermission()
                        }
                        .show()
            }
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView!!.id) {
            R.id.flash_tb -> {
                if (isChecked) {
                    scan_view.setTorchOn()
                } else {
                    scan_view.setTorchOff()
                }
            }
        }
    }

    override fun startProgress() {
        progress_bar.visibility = VISIBLE
        scan_text.visibility = GONE
        scan_view.pause()
    }

    override fun stopProgress() {
        progress_bar.visibility = GONE
        scan_text.visibility = VISIBLE
        scan_view.resume()
    }

    override fun showUpdateAppMessage() {
        scan_view.pause()
        AlertDialog.Builder(this)
                .setTitle(R.string.error_deprecated_version_title)
                .setMessage(R.string.error_deprecated_version)
                .setPositiveButton(R.string.error_dialog_button_download_update) { dialog, _ ->
                    scan_view.resume()
                    ActivityNavigator.goToPlayStore(this)
                    dialog.cancel()
                }
                .setNegativeButton(R.string.error_dialog_button_cancel) { dialog, _ ->
                    scan_view.resume()
                    dialog.cancel()
                }
                .setCancelable(false)
                .show()
    }

    override fun handleError(throwable: Throwable) {
        val message = when (throwable::class) {
            AppErrors.BarcodeScanException::class -> {
                getString(R.string.error_barcode_scan_failed)
            }
            AppErrors.WrongBarcodeFormatException::class -> {
                getString(R.string.error_barcode_format)
            }
            AppErrors.NetworkConnectionException::class -> {
                getString(R.string.error_network_connection)
            }
            AppErrors.ProductInModerationException::class -> {
                getString(R.string.error_product_in_moderation)
            }
            AppErrors.ServerException::class -> {
                when ((throwable as AppErrors.ServerException).code) {
                    "403" -> getString(R.string.error_token)
                    else -> getString(R.string.error_server_scan)
                }
            }
            else -> {
                getString(R.string.error_server_scan)
            }
        }

        scan_view.pause()
        AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.error_dialog_button_ok) { dialog, _ ->
                    scan_view.resume()
                    dialog.cancel()
                }
                .setCancelable(false)
                .show()
    }

    override fun showBanner(ad: Advert) {
        Glide.with(this).load(ad.banner).into(advert_banner_iv)
        advert_banner_iv.setOnClickListener {
            presenter.onAdvertClicked(ad)
        }
    }

    override fun openLink(link: String) {
        ActivityNavigator.goToLink(this, link)
    }

    override fun goToSuitableScreen() {
        animate(R.drawable.ic_suitable, R.color.colorSuitablePrimary)
    }

    override fun goToUnsuitableScreen() {
        animate(R.drawable.ic_unsuitable, R.color.colorUnsuitablePrimary)
    }

    override fun goToGrayAreaScreen() {
        animate(R.drawable.ic_gray_area, R.color.colorGrayAreaPrimary)
    }

    private fun animate(@DrawableRes iconResId : Int, @ColorRes colorResId : Int) {
        TransitionManager.beginDelayedTransition(transitions_container, TransitionSet()
                .addTransition(ChangeBounds().setPathMotion(ArcMotion())
                        .setDuration(300))
                .addTransition(Recolor()
                        .setDuration(300)
                        .addListener(object : Transition.TransitionListener {
                            override fun onTransitionEnd(transition: Transition) {
                                showProductInfo()
                                overridePendingTransition(R.anim.fadein, R.anim.stillness)
                            }
                            override fun onTransitionStart(transition: Transition) {

                            }
                            override fun onTransitionCancel(transition: Transition) {

                            }
                            override fun onTransitionPause(transition: Transition) {

                            }
                            override fun onTransitionResume(transition: Transition) {

                            }
                        }))
        )

        hideViews()
        animateImage(iconResId)
        animateToolbar(colorResId)
    }

    private fun hideViews() {
        progress_bar.visibility = GONE
        scan_frame.visibility = GONE
    }

    private fun animateImage(@DrawableRes iconResId : Int) {
        val params = search_result_image.layoutParams as RelativeLayout.LayoutParams
        params.addRule(RelativeLayout.BELOW, 0)
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0)
        params.addRule(RelativeLayout.CENTER_IN_PARENT, 0)
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            params.addRule(RelativeLayout.ALIGN_PARENT_START)
        }

        search_result_image.layoutParams = params
        search_result_image.setImageResource(iconResId)
    }

    private fun animateToolbar(@ColorRes colorResId : Int) {
        toolbar_title.text = ""
        toolbar.background = ColorDrawable(ContextCompat.getColor(this, colorResId))
    }

    override fun goToNotFoundScreen() {
        showProductInfo()
    }

    private fun showProductInfo() {
        ActivityNavigator.goToProductDisplayActivity(this)
    }
}