package com.maghelyen.vegcode.ui.product

import android.app.ActionBar
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.View.VISIBLE
import android.widget.LinearLayout
import com.maghelyen.vegcode.R
import com.maghelyen.vegcode.VegCodeApplication
import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.product.display.ProductDisplayContract
import com.maghelyen.vegcode.ui.ActivityNavigator
import kotlinx.android.synthetic.main.activity_product_display.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

/**
 * Created by Alena_Kabardinova on 3/19/2018.
 */
class ProductDisplayActivity : AppCompatActivity(), ProductDisplayContract.View, View.OnClickListener {
    companion object {
        const val TAG = "ProductDisplayActivity"
        fun getIntent(context : Context) : Intent = Intent(context, ProductDisplayActivity::class.java)
    }

    var suitable : Product.Suitability = Product.Suitability.UNKNOWN

    @Inject
    lateinit var presenter: ProductDisplayContract.Presenter

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)

        (application as VegCodeApplication).appComponent.newProductDisplayComponent().inject(this)
        presenter.attach(this)

        setContentView(R.layout.activity_product_display)
        presenter.initView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun setSuitableProductTheme() {
        suitable = Product.Suitability.SUITABLE
        setTheme(R.style.SuitableAppTheme)
    }

    override fun setUnsuitableProductTheme() {
        suitable = Product.Suitability.UNSUITABLE
        setTheme(R.style.UnsuitableAppTheme)
    }

    override fun setGrayAreaProductTheme() {
        suitable = Product.Suitability.GRAY
        setTheme(R.style.GrayAreaAppTheme)
    }

    override fun setNotFoundProductTheme() {
        setTheme(R.style.AppTheme)
    }

    override fun init() {

        setSupportActionBar(toolbar)

        supportActionBar?.displayOptions =
                ActionBar.DISPLAY_SHOW_HOME or
                ActionBar.DISPLAY_SHOW_TITLE or
                ActionBar.DISPLAY_HOME_AS_UP or
                ActionBar.DISPLAY_USE_LOGO
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        when (suitable) {
            Product.Suitability.SUITABLE -> {
                supportActionBar?.setDisplayShowTitleEnabled(false)
                toolbar_title.setText(R.string.screen_suitable_title)
                toolbar_icon.visibility = VISIBLE
                toolbar_icon.setImageResource(R.drawable.ic_suitable)
            }
            Product.Suitability.UNSUITABLE -> {
                supportActionBar?.setDisplayShowTitleEnabled(false)
                toolbar_title.setText(R.string.screen_unsuitable_title)
                toolbar_icon.visibility = VISIBLE
                toolbar_icon.setImageResource(R.drawable.ic_unsuitable)
            }
            Product.Suitability.GRAY -> {
                supportActionBar?.setDisplayShowTitleEnabled(false)
                toolbar_title.setText(R.string.screen_gray_area_title)
                toolbar_icon.visibility = VISIBLE
                toolbar_icon.setImageResource(R.drawable.ic_gray_area)
            }
            Product.Suitability.UNKNOWN -> toolbar_title.setText(R.string.app_name)
        }

        edit_product_btn.setOnClickListener(this)
        new_scan_btn.setOnClickListener(this)
    }

    override fun displayProductInfo(
            name : String,
            manufacturer : String,
            country: String,
            ingredients : String,
            issues : ArrayList<Product.Issue>
    ) {
        name_tv.text = name
        manufacturer_tv.text = manufacturer
        ingredients_tv.text = String.format(getString(R.string.ingredients_text), ingredients)

        issues_container.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        val adapter = IssuesAdapter(issues)
        issues_container.adapter = adapter

        edit_product_btn.text = getString(R.string.suggest_correction_button)
    }

    override fun displayProductNotFound() {
        name_tv.text = getString(R.string.product_not_found_title)
        product_not_found_tv.visibility = VISIBLE
        edit_product_btn.text = getString(R.string.add_product_info_button)
    }

    override fun goToEditScreen() {
        ActivityNavigator.goToProductEditActivity(this)
    }

    override fun goToScanScreen() {
        ActivityNavigator.goToParentActivity(this)
    }

    override fun onBackPressed() {
        ActivityNavigator.goToParentActivity(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.edit_product_btn -> presenter.editButtonClicked()
            R.id.new_scan_btn -> presenter.newScanButtonClicked()
        }
    }
}