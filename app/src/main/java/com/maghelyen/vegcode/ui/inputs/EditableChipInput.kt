package com.maghelyen.vegcode.ui.inputs

import android.content.Context
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout.VERTICAL
import android.widget.TextView
import com.google.android.flexbox.FlexboxLayout
import com.jakewharton.rxbinding2.widget.RxTextView
import com.maghelyen.vegcode.R
import com.maghelyen.vegcode.ui.inputs.chipsinput.SuggestionsAdapter
import kotlinx.android.synthetic.main.chip_input_view.view.*
import java.util.concurrent.TimeUnit

/**
 * Created by Alena_Kabardinova on 5/14/2018.
 */
class EditableChipInput :
        FrameLayout,
        SuggestionsAdapter.SuggestionsListener,
        EditableInput<ArrayList<String>> {
    private var listen = true
    private var editListener : EditableInput.OnEditListener? = null
    private var querySuggestionsListener: OnQuerySuggestionsListener? = null

    private val data : ArrayList<String> = ArrayList()
    private var suggestions : ArrayList<String> = ArrayList()

    interface OnQuerySuggestionsListener {
        fun onQuerySuggestions(query : String, input : EditableChipInput)
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet? = null) {
        inflate(context, R.layout.chip_input_view, this)

        if (attrs != null) {
            search_til.hint = context.getString(attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "hint", R.string.empty))
        }
        search_et.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val chip = search_et.text?.trim()
                addChip(chip.toString())
            }
        }
        RxTextView.textChanges(search_et)
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe({
                    querySuggestionsListener?.onQuerySuggestions(it.toString(), this)
                })

        RxTextView.textChanges(search_et)
                .subscribe({
                    val dividers = "[.,]"

                    if (it.contains(dividers.toRegex())) {
                        val chips = it.split(dividers.toRegex())
                        chips.forEach {
                            addChip(it.trim())
                        }
                    }
                })

        auto_complete_list.layoutManager = LinearLayoutManager(context, VERTICAL, false)
        auto_complete_list.adapter = SuggestionsAdapter(suggestions, this)
        ViewCompat.setElevation(auto_complete_list, 10f)
    }

    private fun createChip(chip : String): View {
        val view = View.inflate(context, R.layout.chip_item_view, null)

        val lp = FlexboxLayout.LayoutParams(
                FlexboxLayout.LayoutParams.WRAP_CONTENT,
                FlexboxLayout.LayoutParams.WRAP_CONTENT
        )
        lp.setMargins(
                resources.getDimension(R.dimen.half_small_margin).toInt(),
                resources.getDimension(R.dimen.half_small_margin).toInt(),
                resources.getDimension(R.dimen.half_small_margin).toInt(),
                resources.getDimension(R.dimen.half_small_margin).toInt()
        )

        view.layoutParams = lp

        view.findViewById<TextView>(R.id.chip_tv).text = chip
        view.findViewById<ImageView>(R.id.delete_iv).setOnClickListener({
            (view.parent as ViewManager).removeView(view)

            data.remove(chip)
            onDataChanged()
        })

        return view
    }

    private fun addChip(chip : String) {
        if (chip.isNotEmpty()) {
            search_et.setText("")
            suggestions.clear()
            onSuggestionsChanged()

            chips_container.addView(createChip(chip), 0)
            data.add(0, chip)
            onDataChanged()
        }
    }

    override fun setValue(value: ArrayList<String>) {
        listen = false
        data.clear()
        data.addAll(value)
        data.forEach {
            chips_container.addView(createChip(it))
        }
        onDataChanged()
        listen = true
    }

    override fun getValue(): ArrayList<String> {
        return data
    }

    override fun setOnEditListener(listener: EditableInput.OnEditListener) {
        editListener = listener
    }

    private fun onDataChanged() {
        chips_container.visibility = if (data.isEmpty()) View.GONE else View.VISIBLE

        if (listen) {
            editListener?.onEdit()
        }
    }

    private fun onSuggestionsChanged() {
        auto_complete_list.adapter?.notifyDataSetChanged()
        auto_complete_list.visibility = if (suggestions.isEmpty()) View.GONE else View.VISIBLE
    }

    fun setOnQueryListener(listener: OnQuerySuggestionsListener) {
        querySuggestionsListener = listener
    }

    fun setSuggestions(suggestions : ArrayList<String>) {
        val filteredSuggestions = suggestions.filter { !data.contains(it) }
        this.suggestions.clear()
        this.suggestions.addAll(filteredSuggestions)
        onSuggestionsChanged()
    }

    override fun onSuggestionClick(suggestion: String) {
        addChip(suggestion)
    }
}