package com.maghelyen.vegcode.interactors

/**
 * Created by Alena_Kabardinova on 6/12/2018.
 */
interface Provider {
    fun getInteractor() : Interactor
}