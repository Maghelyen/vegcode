package com.maghelyen.vegcode.interactors.di

import android.content.Context
import com.maghelyen.vegcode.interactors.InteractorProvider
import com.maghelyen.vegcode.interactors.LocalDBInteractor
import com.maghelyen.vegcode.interactors.ServerInteractor
import com.maghelyen.vegcode.di.AppScope
import com.maghelyen.vegcode.interactors.Provider
import com.maghelyen.vegcode.network.VegCodeAPIService
import com.maghelyen.vegcode.user.UserRepo
import dagger.Module
import dagger.Provides

/**
 * Created by Alena_Kabardinova on 5/27/2018.
 */
@Module
class DataModule {

    @Provides
    @AppScope
    fun provideInteractorProvider(
            context : Context,
            serverInteractor : ServerInteractor,
            localDBInteractor : LocalDBInteractor
    ) : Provider =
            InteractorProvider(context, serverInteractor, localDBInteractor)

    @Provides
    @AppScope
    fun provideServerInteractor(
            context : Context,
            userRepo : UserRepo,
            vegCodeAPIService : VegCodeAPIService
    ) : ServerInteractor =
            ServerInteractor(context, userRepo, vegCodeAPIService)

    @Provides
    @AppScope
    fun provideLocalDBInteractor() : LocalDBInteractor =
            LocalDBInteractor()
}