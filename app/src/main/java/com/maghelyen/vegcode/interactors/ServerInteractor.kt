package com.maghelyen.vegcode.interactors

import android.content.Context
import android.content.pm.PackageManager
import com.maghelyen.vegcode.AppErrors
import com.maghelyen.vegcode.advertising.Advert
import com.maghelyen.vegcode.network.VegCodeAPIService
import com.maghelyen.vegcode.network.ProductRequest
import com.maghelyen.vegcode.product.Product
import com.maghelyen.vegcode.user.UserRepo
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Alena_Kabardinova on 5/27/2018.
 */
class ServerInteractor(
        context: Context,
        userRepo: UserRepo,
        private val vegCodeAPIService : VegCodeAPIService
) : Interactor {

    private val versionNumber : Int
    private val token : String
    private val category : String
    private val issueCategory : String
    private val userId : String
    private val source : String

    init {
        versionNumber = try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            pInfo.versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            0
        }

        userId = userRepo.getUserId()

        token = "792fec82e3a0dcea1817fd9ebfaf1533"
        category = "vegan"
        issueCategory = "product"
        source = "android"
    }

    override fun getProductByBarcode(barcode : String) : Single<Product> {
        return vegCodeAPIService.getProduct(token, category, barcode, versionNumber, source)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if (it.success) {
                        return@map it.product
                    }
                    throw when (it.error!!.code) {
                        "402" -> AppErrors.ProductInModerationException()
                        "404" -> AppErrors.ProductNotFoundException()
                        "407" -> AppErrors.WrongBarcodeFormatException()
                        "410" -> AppErrors.DeprecatedAppVersion()
                        else -> AppErrors.ServerException(it.error.code,
                                it.error.code + " - " + it.error.message + ". Parameters: token: "
                                        + token + ", category: " + category + ", barcode: " + barcode)
                    }
                }
    }

    override fun getProductIssues(): Single<ArrayList<Product.Issue>> {
        return vegCodeAPIService.getIssues(token, category, issueCategory, versionNumber, source)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if (it.success) {
                        return@map it.issues
                    }
                    throw when (it.error!!.code) {
                        "410" -> AppErrors.DeprecatedAppVersion()
                        else -> AppErrors.ServerException(it.error!!.code,
                                it.error.code + " - " + it.error.message +
                                        ". Parameters: token: " + token +
                                        ", category: " + category +
                                        ", issueCategory" + issueCategory)
                    }
                }
    }

    override fun searchIngredients(searchQuery: String): Single<ArrayList<String>> {
        return vegCodeAPIService.getIngredients(token, searchQuery, versionNumber, source)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if (it.success) {
                        return@map it.ingredients
                    }
                    throw when (it.error!!.code) {
                        "410" -> AppErrors.DeprecatedAppVersion()
                        else -> AppErrors.ServerException(it.error!!.code,
                                it.error.code + " - " + it.error.message +
                                        ". Parameters: token: " + token +
                                        ", searchQuery: " + searchQuery)
                    }
                }
    }

    override fun saveProduct(barcode: String, product: Product, comment: String): Single<Boolean> {
        return vegCodeAPIService.addProduct(ProductRequest(barcode, comment, product, token, versionNumber, source))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if (it.success) {
                        return@map true
                    }
                    throw when (it.error!!.code) {
                        "410" -> AppErrors.DeprecatedAppVersion()
                        else -> AppErrors.ServerException(it.error!!.code,
                                it.error.code + " - " + it.error.message +
                                        ". Parameters: token: " + token +
                                        ", barcode: " + barcode +
                                        ", comment: " + comment +
                                        ", product: " + product)
                    }
                }
    }

    override fun getAdvert(space: String, size: String): Single<Advert> {
        return vegCodeAPIService.getAd(token, userId, space, size, versionNumber, source)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if (it.success) {
                        return@map it.advert
                    }
                    throw when (it.error!!.code) {
                        "410" -> AppErrors.DeprecatedAppVersion()
                        else -> AppErrors.ServerException(it.error.code,
                                it.error.code + " - " + it.error.message +
                                        ". Parameters: token: " + token +
                                        ", userId: " + userId +
                                        ", space: " + space +
                                        ", size: " + size)
                    }
                }
    }

    override fun advertClicked(space: String, adId: Int) : Single<Boolean> {
        return vegCodeAPIService.adLinkOpened(token, userId, adId, space, versionNumber, source)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    if (it.success) {
                        return@map true
                    }
                    throw AppErrors.ServerException(it.error?.code!!,
                            it.error.code + " - " + it.error.message +
                                    ". Parameters: token: " + token +
                                    ", userId: " + userId +
                                    ", adId: " + adId +
                                    ", space: " + space)
                    }
    }
}