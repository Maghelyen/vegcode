package com.maghelyen.vegcode.interactors

import com.maghelyen.vegcode.advertising.Advert
import com.maghelyen.vegcode.product.Product
import io.reactivex.Single

/**
 * Created by Alena_Kabardinova on 5/27/2018.
 */
interface Interactor {
    fun getProductByBarcode(barcode : String) : Single<Product>
    fun getProductIssues() : Single<ArrayList<Product.Issue>>
    fun searchIngredients(searchQuery : String) : Single<ArrayList<String>>
    fun saveProduct(barcode : String, product : Product, comment : String) : Single<Boolean>
    fun getAdvert(space : String, size: String) : Single<Advert>
    fun advertClicked(space: String, adId : Int) : Single<Boolean>
}