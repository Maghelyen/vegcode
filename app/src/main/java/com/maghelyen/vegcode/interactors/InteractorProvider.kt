package com.maghelyen.vegcode.interactors

import android.content.Context
import android.net.ConnectivityManager
import com.maghelyen.vegcode.AppErrors

/**
 * Created by Alena_Kabardinova on 5/27/2018.
 */
class InteractorProvider(
        private val context: Context,
        private val serverInteractor: ServerInteractor,
        private val localDBInteractor: LocalDBInteractor
) : Provider {

    override fun getInteractor() : Interactor {
        /*
        Here will be the internet connection check that would determine
        what data source to use: server or local db
        Right now we will always use server or throw en Exception if
        device is not connected to the internet
        */
        if (isNetworkConnected()) {
            return serverInteractor
        } else {
            throw AppErrors.NetworkConnectionException()
        }
    }

    private fun isNetworkConnected(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null
    }
}