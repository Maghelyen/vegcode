package com.maghelyen.vegcode.interactors

import com.maghelyen.vegcode.advertising.Advert
import com.maghelyen.vegcode.product.Product
import io.reactivex.Single

/**
 * Created by Alena_Kabardinova on 5/27/2018.
 */
class LocalDBInteractor : Interactor {
    override fun getProductByBarcode(barcode: String): Single<Product> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getProductIssues(): Single<ArrayList<Product.Issue>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun searchIngredients(searchQuery: String): Single<ArrayList<String>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveProduct(barcode: String, product: Product, comment: String): Single<Boolean> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAdvert(space: String, size: String): Single<Advert> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun advertClicked(space: String, adId: Int) : Single<Boolean> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}